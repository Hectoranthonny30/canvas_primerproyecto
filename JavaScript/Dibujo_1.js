// JavaScript Document
var lienzo = document.querySelector("canvas");
var ctx = lienzo.getContext("2d");


//TECHO_1

ctx.beginPath();

ctx.moveTo(0,150);
ctx.lineTo(80,45);
ctx.lineTo(150,150);
ctx.lineTo(135,150);
ctx.lineTo(75,50);


ctx.moveTo(0,150)
ctx.lineTo(15,150);
ctx.lineTo(85,50);


ctx.lineWidth=5;
ctx.strokeStyle='rgba(250, 166, 71, 1)';
ctx.stroke();

//MURO_1

ctx.beginPath();

ctx.moveTo(10,150);
ctx.lineTo(10,275);
ctx.lineTo(145,275);
ctx.lineTo(145,150);


ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.stroke();

//MURO_2

ctx.beginPath();

ctx.moveTo(145,268);
ctx.lineTo(355,268);
ctx.lineTo(355,125);


ctx.lineWidth=5;
ctx.stroke();

//TECHO_2
ctx.beginPath();

ctx.moveTo(135,125);
ctx.lineTo(365,125);
ctx.lineTo(350,50);
ctx.lineTo(85,50);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(250, 166, 71, 1)';
ctx.stroke();

//CHIMENEA_1
ctx.beginPath();

ctx.moveTo(320,50);
ctx.lineTo(320,20);
ctx.lineTo(285,20);
ctx.lineTo(285,50);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.stroke();

//CHIMENEA_2
ctx.beginPath();

ctx.moveTo(330,20);
ctx.lineTo(275,20);
ctx.lineTo(275,10);
ctx.lineTo(330,10);
ctx.lineTo(330,22.5);

ctx.lineWidth=5;
ctx.stroke();

//LINEAVENTANA_1

ctx.beginPath();

ctx.moveTo(35,167);
ctx.lineTo(68,167);

ctx.lineWidth=5;
ctx.stroke();

//LINEAVENTANA_2

ctx.beginPath();

ctx.moveTo(85,167);
ctx.lineTo(118,167);

ctx.lineWidth=5;
ctx.stroke();

//LINEAVENTANA_3

ctx.beginPath();

ctx.moveTo(35,217);
ctx.lineTo(65,217);

ctx.lineWidth=5;
ctx.stroke();

//LINEAVENTANA_4

ctx.beginPath();

ctx.moveTo(85,217);
ctx.lineTo(118,217);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.stroke();

//ARCOPUERTA

ctx.beginPath();

ctx.moveTo(300,203);
ctx.arc(250,203,55,0,Math.PI,true);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.stroke();

ctx.beginPath();

ctx.moveTo(300,203);
ctx.arc(250,203,35,0,Math.PI,true);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.stroke();

//VENTANA_1

ctx.beginPath();

ctx.fillStyle="transparent";
ctx.fillRect(35,152,30,30);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.rect(35,152,30,30);
ctx.stroke();

//VENTANA_2

ctx.fillStyle="transparent";
ctx.fillRect(85,152,30,30);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.rect(85,152,30,30);
ctx.stroke();

//VENTANA_3

ctx.fillStyle="transparent";
ctx.fillRect(85,202,30,30);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.rect(85,202,30,30);
ctx.stroke();

//VENTANA_4

ctx.fillStyle="transparent";
ctx.fillRect(35,202,30,30);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.rect(35,202,30,30);
ctx.stroke();

//VENTANA_5

ctx.fillStyle="transparent";
ctx.fillRect(155,168,30,65);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.rect(155,168,30,65);
ctx.stroke();

//VENTANA_6

ctx.fillStyle="transparent";
ctx.fillRect(315,168,30,65);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.rect(315,168,30,65);
ctx.stroke();

//PUERTA

ctx.fillStyle="transparent";
ctx.fillRect(225,203,50,65);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.rect(225,203,50,65);
ctx.stroke();

//PUERTA_LINEA1

ctx.fillStyle="transparent";
ctx.fillRect(235,213,30,15);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.rect(235,213,30,15);
ctx.stroke();

//PUERTA_LINEA2

ctx.fillStyle="transparent";
ctx.fillRect(235,240,30,20);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.rect(235,240,30,20);
ctx.stroke();

//COLUMNA_1

ctx.fillStyle="transparent";
ctx.fillRect(200,203,10,65);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.rect(200,203,10,65);
ctx.stroke();

//COLUMNA_2

ctx.fillStyle="transparent";
ctx.fillRect(290,203,10,65);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.rect(290,203,10,65);
ctx.stroke();

//ESCALERA_1

ctx.fillStyle="black";
ctx.fillRect(200,268,100,15);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.rect(200,268,100,15);
ctx.stroke();

//ESCALERA_2

ctx.fillStyle="black";
ctx.fillRect(195,285,110,15);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(34,34,34,0.75)';
ctx.rect(195,285,110,15);
ctx.stroke();

//TRONCO_1

ctx.fillStyle="brown";
ctx.fillRect(375,218,25,75);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(128,0,0))';
ctx.rect(375,218,25,75);
ctx.stroke();

//TRONCO_2

ctx.fillStyle="brown";
ctx.fillRect(415,248,20,45);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(128,0,0))';
ctx.rect(415,248,20,45);
ctx.stroke();

//TRONCO_3

ctx.fillStyle="brown";
ctx.fillRect(450,258,15,35);

ctx.lineWidth=5;
ctx.strokeStyle='rgba(128,0,0))';
ctx.rect(450,258,15,35);
ctx.stroke();

//HOJAS PRIMER ARBOL

ctx.beginPath();
ctx.ellipse(388, 200, 25, 15, 0, 0, 2 * Math.PI, false);
ctx.fillStyle = "green";
ctx.fill();
ctx.stroke();

//HOJAS SEGUNDO ARBOL

ctx.beginPath();
ctx.arc(425,230,15,0,2*Math.PI);

ctx.fillStyle="green";
ctx.fill();

//HOJAS TERCER ARBOL

ctx.beginPath();
ctx.arc(458,245,10,0,2*Math.PI);

ctx.fillStyle="green";
ctx.fill();

//TITULO
ctx.beginPath();

ctx.font = "15px Arial Black";
ctx.fillStyle = "black";
ctx.fillText("My home", 400, 80);
ctx.fill();
